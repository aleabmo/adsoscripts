#!/bin/bash python

import sys
import time
import subprocess

#with open('./netstat', 'r') as net:
while True:
    paq_total=0
    #Creem una llista que contingui les interficies actives
    net = subprocess.Popen("netstat -i | cut -d' ' -f1 | tail -n +3", shell=True, stdout=subprocess.PIPE).stdout.read()
    line = net.strip().split("\n")
    #Iterem sobre la llista per tal de obtenir les dades enviades
    for iface in line:
        data = subprocess.Popen("netstat -i | grep "+iface+" | awk '{print $7;}' ", shell=True, stdout=subprocess.PIPE).stdout.read()
        print iface+":  "+data.strip()
        paq_total+=int(data.strip())
    print "Total:   ",paq_total
    #Sleep cada n segons
    time.sleep(float(sys.argv[1]))
