#!/usr/bin/env python

import sys
import os
from pathlib import Path

usage="python ocupacio.py n[K,M,G]"
missatge="echo -e \"Missatge ocupacio: T estas flipan amb la cap\nEliminar missatge: (sed -i '/ocupacio/d' ~/.bash_profile)\" "
#Llegim el fitxer passwd, busquem al user i guardem valors
if (len(sys.argv) == 2):
    #Si es la opcio per defecte, carreguem tot els users del fitxer /etc/passwd, que tinguin un directory a partir del /home
    with open('/etc/passwd') as file:
        users=[ [line.split(':')[0], line.split(':')[2], line.split(':')[5]]  for line in file if "/home" in line.split(':')[5] ]
        parse=sys.argv[1]

elif (len(sys.argv) == 4):
    #Si es la opcio de grup, carreguem a la variable users, els usuaris que siguin d'aquest grup
    with open('/etc/group') as file:
        for line in file:
            if sys.argv[2] in line:
                uid_grup = line.split(':')[2] 
                break

    with open('/etc/passwd') as file:
        try:
            users=[ [line.split(':')[0], line.split(':')[2], line.split(':')[5]]  for line in file if uid_grup == line.split(':')[3] ]
        except:
            print "El grup no es correcte"
            sys.exit(1)
        parse=sys.argv[3]

else:
    print usage
    sys.exit(1)

if (parse[-1:] == "K"):
    cap=int(parse[:-1]) * 1024
elif (parse[-1:] == "M"):
    cap=int(parse[:-1]) * 1024 * 1024
elif (parse[3][-1:] == "G"):
    cap=int(parse[3][:-1]) * 1024 * 1024
else:
    print usage
    sys.exit(1)

for user in users:
        id_user = user[0]
        uid_user = user[1]
        home = user[2]

        if Path(home).is_dir() :
            total_size = 0
            for path, dirs, files in os.walk(home):
                for f in files:
                    fp=os.path.join(path, f)
                    try:
                        if (os.stat(fp).st_uid == int(uid_user)):
                            total_size += os.path.getsize(fp)
                    except:
                        pass

            if (total_size > cap):
                #Per tal de fer proves rapides
                print "MAL: "+id_user, total_size
                with open(home+'/.bash_profile', 'a') as my_file:
                    my_file.write(missatge)
            else:
                print "BO: "+id_user, total_size



