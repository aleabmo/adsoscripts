#!/usr/bin/env python

import sys
import os
import psutil

with open('/etc/passwd') as file:
    for line in file:
        if sys.argv[1] == line.split(':')[0]:
            home = line.split(':')[5]
            uid = line.split(':')[2]
            break

total_size = 0
for path, dirs, files in os.walk(home):
    for f in files:
        fp = os.path.join(path, f)
        total_size += os.path.getsize(fp)

print "Home: ", home

if (total_size > 1024):
	total_size = total_size / 1024
	if (total_size > 1024):
		total_size = total_size / 1024
		if (total_size > 1024):
			total_size = total_size / 1024
			print "Home size: ", str(total_size),"GB"
		else:
			print "Home size: ", str(total_size),"MB"
	else:
		print "Home size: ", str(total_size),"KB"
else:
	print "Home size: ", str(total_size),"B"


print "Other dirs: "
for path, dirs, files in os.walk('/'):
    dirs[:] = [d for d in dirs if d not in home]
    for f in dirs:
        fp = os.path.join(path, f)
        if (os.stat(fp).st_uid == int(uid)):
            print fp

numero=len([p.username() for p in psutil.process_iter() if p.username() == sys.argv[1]])
print "Active processes: ", numero
