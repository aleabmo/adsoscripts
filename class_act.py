#!/usr/bin/env python

import sys
import os
import time

#Llegim el fitxer passwd, busquem al user i guardem valors
with open('/etc/passwd') as file:
    for line in file:
        #Busquem a line per tal de poder fer proves, sino line.split(':')[6]
        if sys.argv[2] in line:
            home = line.split(':')[5]
            id_user = line.split(':')[0]
            break

#Calculem la diferencia de temps dels arxius que es troben al home del user
total_size = 0
contador = 0
for path, dirs, files in os.walk(home):
    for f in files:
        fp = os.path.join(path, f)
        temps_ara= time.time()
        if ((temps_ara - os.stat(fp).st_mtime) / (60*60* 24) < int(sys.argv[1])):
            contador+=1
            total_size += os.path.getsize(fp)

#Condicions per tal de pasar de B a alguna cosa mes llegible
if (total_size > 1024):
    total_size = total_size / 1024
    if (total_size > 1024):
        total_size = total_size / 1024
        if (total_size > 1024):
            total_size = total_size / 1024
            resultat=str(total_size)+"GB"
        else:
            resultat=str(total_size)+"MB"
    else:
        resultat=str(total_size)+"KB"
else:
    resultat=str(total_size)+"B"


print sys.argv[2]+" ("+id_user+") ", contador, "fitxers modificats que ocupen "+ resultat

