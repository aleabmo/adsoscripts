#!/bin/bash
htemps=0
mtemps=0
if [ $# -eq 0 ];then
	echo "Resum de logins:"
	for user in $(cat /etc/passwd | grep "home" | cut -d: -f1); do
		htemps=0
		mtemps=0
		for hora in $(last $user | grep -o "(..:..)" | grep -o "..:.." | cut -d: -f1);do
			((htemps+=10#$hora))
		done
		for min in $(last $user | grep -o "(..:..)" | grep -o "..:.." | cut -d: -f2);do
			((mtemps+=10#$min))
		done
		logins=$(last $user | grep -o "(..:..)" | grep -o "..:.." | wc -l)
		echo "Usuari $user: temps total hores:$htemps, min:$mtemps, $logins"
	done

	echo -e "\n\nResum d'usuaris connectats"
	for user in $(users); do
		cpu=0
		proces=$(ps -u $user --no-headers | wc -l)
		for x in $(ps -u $user --no-headers -o %cpu | cut -d. -f1);do
			((cpu+=$x))
		done
		echo "Usuari $user: $proces processos -> $cpu % CPU"
	done
fi
