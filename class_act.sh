#!/bin/bash
usage="Usage: class_act.sh [n days] \"Full Name\""
# detecció de opcions d'entrada: només son vàlids: sense paràmetres i -p
if [ $# -ne 2 ]; then
	echo $usage; exit 1
fi

user=$(cut -d: -f1,5 /etc/passwd | grep "$2" | cut -d: -f1)
home=$(cat /etc/passwd | grep "^$user\>" | cut -d: -f6)
fitxers=$(find $home -type f -user $user -mtime -$1)
cont=$(echo $fitxers | wc -w)
accCap=$(du -hsc $fitxers | tail -n -1 | awk '{print $1;}')
echo "$2 ($user) $cont fitxers modificats que ocupen $accCap"
