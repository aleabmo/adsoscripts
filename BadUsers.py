#!/usr/bin/env python

import sys
import os
import time
from pathlib import Path
import subprocess

usage="python BadUsers.py [-p/-t n[d,m,y]"
arg=0
#Llegim el fitxer passwd, busquem al user i guardem valors
if (len(sys.argv) > 1):
    if (sys.argv[1] == "-t"):
        arg=2
        if (sys.argv[2][-1:] == "d"):
            dies=str(sys.argv[2][:-1])
        elif (sys.argv[2][-1:] == "m"):
            dies=str(int(sys.argv[2][:-1])*30)
        elif (sys.argv[2][-1:] == "y"):
            dies=str(int(sys.argv[2][:-1])*365)
        else:
            print usage
            sys.exit(1)
    elif ( sys.argv[1] == "-p" ):
        arg=1

with open('/etc/passwd') as file:
    for line in file:
        home = line.split(':')[5]
        id_user = line.split(':')[0]
        uid_user = line.split(':')[2]
        if Path(home).is_dir():
            num_fich=0
            for path, dirs, files in os.walk(home):
                for f in files:
                    fp=os.path.join(path, f)
                    try:
                        if (os.stat(fp).st_uid == int(uid_user)):
                            num_fich+=1
                    except:
                        pass
        if (num_fich == 0):
            if (arg == 1):
                user_proc = subprocess.Popen("ps -u "+id_user+" | tail -n +2 | wc -l", shell=True, stdout=subprocess.PIPE).stdout.read()
                if (int(user_proc) == 0):
                    print id_user
            else:
                print id_user

        else:
            if (arg == 2):
                contador=0
                last_log=0
                for path, dirs, files in os.walk(home):
                    for f in files:
                        fp = os.path.join(path, f)
                        temps_ara= time.time() 
                        try:
                            if ((temps_ara - os.stat(fp).st_mtime) / (60*60* 24) < int(sys.argv[2])):
                                contador+=1
                        except:
                            pass
                lastlog = subprocess.Popen("last -w "+id_user+" -s -"+dies+"days | head -n -3 | wc -l", shell=True, stdout=subprocess.PIPE).stdout.read()
                if (contador == 0 and int(lastlog) == 0):
                    print id_user



